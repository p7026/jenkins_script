# !/bin/bash 

# Programa para instalar Jenkins en local.
# Author: Camilo Barrera Bolivar

#Prevalidación de dependencias
wget_version=$(which wget)
curl_version=$(which curl)
if [ -z "$wget_version" ]; then 
sudo apt-get install wget 
elif [ -z "$curl_version" ]; then
sudo apt-get install curl
fi

#Ingreso de contraseña
echo "Ingrese su contraseña: "
read -s password
echo $password
#Instalación de Jenkins
echo "------------------------installing jenkins--------------------------"
curl -fsSL https://pkg.jenkins.io/debian-stable/jenkins.io.key | sudo tee \
  /usr/share/keyrings/jenkins-keyring.asc > /dev/null
echo deb [signed-by=/usr/share/keyrings/jenkins-keyring.asc] \
  https://pkg.jenkins.io/debian-stable binary/ | sudo tee \
  /etc/apt/sources.list.d/jenkins.list > /dev/null
sudo apt-get update
sudo apt-get install jenkins
echo"------------------------finish jenkins instalation--------------------"
# Instalación de java
echo "-----------installing Java-------------------------------------------"
sudo apt update
sudo apt install openjdk-11-jre
java -version
echo "-----------finish Java instalation-----------------------------------"

